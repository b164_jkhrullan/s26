let http = require("http");

http.createServer(function(request, response) {

	response.writeHead(200, {'Content-Type': 'application/json'});
	response.end("Hello World")

}).listen(4000);

console.log('Server running at localhost:4000')



// Status Code
// 404 - not found
// 200 - ok
// 100 - 199 -Informational responses
// 200 - 299 - Successful responses 
// 300 - 399 - Redirection messages 